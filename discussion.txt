CLI Commands
mkdir - create a new folder/directory
	syntax:
		mkdir <folderName>
		ex. mkdir batch-197
touch - used to create files
	syntax:
		touch <fileName>
		ex. touch discussion.txt
cd - change directory
	- change the current folder/directory we are currently working on with our CLI (terminal/gitbash)
	syntax:
		cd <folderName/path to folder>
		ex. cd Documents
cd .. - it moves back one folder/directory
	syntax:
		cd ..
ls - list 
	- it list files and folders contained by the current directory
	syntax:
		ls
pwd - present working directory
	- shows the current directory we are working on
	syntax:
		pwd
When using git for the first in computer/machine

Configure our Git:
	- git config --global user.email <emailFromGitlab>
	- this will allow us to identify the account from gitlab/github who will push/upload files into our online GitLab or Github services

	- git config --global user.name "<nameUsedInGitLab>"
	- this will allow us to identify the name of the user who is trying to upload/push files into our online GitLab or GitHub services.
	- this allow us to determine the name of the person who uploaded the lates commit/version in our repo

What is SSH key?
	- SSH or Secure Shell Key 
		- tools used to authenticate the uploading or of other tasks when manipulating or using git repositories
		- it allows us to push or upload into our online git repo without the use of passwords

	ssh-keygen -t ed25519
		- t option to select algorithm
		- ed25519 is the new algo added in OpenSSH

Git commands - pushing/uploading:

Pushing for the very first time:

git init - allows us to initialize a local folder as a local git repo. This means that the folder and its files is now tracked by git. You can then upload/push the updates tracked in the folder into your online repo.

git add . - allows us to add all files and updates into a new commit/version of our files and folders to be uploaded to our online repo.

git commit -m "<commitMessage>" - This will allow us to create a new version of our files and folders based on the updates added using git add .
We also are able to add a proper and approriate title/message that defines the changes/updates made to files and folders. 
	-commit messages usually start with a verb (add discussions,update index.html, etc.)

git remote add origin <gitURLOfOnlineRepo> - this will us to connect an online repo to a local repo. "origin" is the default/conventional name for an online repo. Local repos can connect to multiple online repos, by convention, the first online repo connected is called "origin".

git push origin master - allows us to push/upload the latest commit created into our online repo that is designated as "origin". master is the branch to upload/push our commit to. master branch it is the default version or the main version of our files in the repo.


Pushing Flows:

When Pushing for the first time:

git status -> git init -> git add . -> git status -> git commit -m "<commitMessage>" -> git status -> git remote add origin <gitURLForOnlineRepo> -> git remote -v -> git push origin master

git init -> git add . -> git commit -m "<commitMessage>" -> git remote add origin <gitURLForOnlineRepo> -> git push origin master

When Pushing updates:

git add . -> git commit -m "<commitMessage>" -> git push origin master



